import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { ConnexionService } from '../services/connexion-service';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {

  authStatus: boolean;
  isErreur:boolean = false;
  isConn:boolean = false;
  constructor(private http: HttpClient, private router: Router, private conService : ConnexionService) { }

  ngOnInit() {
    this.authStatus = this.conService.isAuth;
  }
  onSubmit(form: NgForm) {
    this.isConn = true;
    this.isErreur = false;
    console.log(form.value);
    const url = 'http://localhost:5555/connexion';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'apllication/json',
        'Access-Control-Allow-Origin':'*'
      })
    };
    this.http.post(url, form.value, httpOptions).subscribe((response) => {
      if(response!=null){
        this.conService.signIn().then(
          () => {
            let propriete:any;
            propriete = response;
            console.log(propriete.nom);
            console.log('Sign in successful! : '+response);
            this.authStatus = this.conService.isAuth;
            this.conService.nom = propriete.nom+" "+propriete.prenom;
            this.conService.id = propriete.id;
            //setTimeout(function(){ this.router.navigate(['incidents']); }, 1000);
            this.router.navigate(['incidents']);
            
          }
        );
      }else{
        this.isConn = false;
        this.isErreur = true;
      }
      
      console.log("Response : ", response);
   },
   (error) => {
      this.isConn = false;
      this.isErreur = true;
      console.error("Erreur : ", error);
   });
}

}
