import { Component, OnInit, Renderer2, OnDestroy, ComponentFactoryResolver, ViewChild, ViewContainerRef, AfterViewInit, ComponentRef, ComponentFactory, Input} from '@angular/core';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import $ from 'jquery';
import { HttpClient } from '@angular/common/http';
import { IncidentViewComponent } from '../incident-view/incident-view.component';
import { Subscription } from 'rxjs';
import { IncidentsService } from '../services/incidents-service';
import { ConnexionService } from '../services/connexion-service';

@Component({
  selector: 'app-liste-incidents',
  templateUrl: './liste-incidents.component.html',
  styleUrls: ['./liste-incidents.component.css']
})
export class ListeIncidentsComponent implements OnInit, OnDestroy{
  
  
  private serverUrl = 'http://localhost:5555/innojam/socket-connect'
  //private title = 'WebSockets chat';
  private stompClient;
  private incidentUrl = "http://localhost:5555/incident/"
  test={
    "id":"",
    "status":"",
    "personne":"",
    "isincident":"",
    "probabilite":""
  }
  proba:Number;
  isProba:boolean;
  isIncidents:boolean=true;
  incidents: any[];
  incidentSubscription: Subscription;
  /*modalSpec:string='<div class="modal-content">'
    +'<div class="modal-header">'
      +'<button type="button" class="close" data-dismiss="modal">&times;</button>'
      +'<h4 class="modal-title">Modal Header</h4>'
    +'</div>'
    +'<div class="modal-body">'
      +'<p>Some text in the modal.</p>'
    +'</div>'
    +'<div class="modal-footer">'
      +'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'
    +'</div>'
  +'</div>';*/
  @Input()
  idIncident:number;
  nom = "";
  @Input()
  tIncident:number=0;
  @Input()
  sIncident:number=0;
  @Input()
  pIncident:number=0;
  modalText:string="mon text du modal";
  modalTitle:string="Modal Title ex"; 

  /*@ViewChild('incidents', {
    read: ViewContainerRef, static: false
  }) target*/
  cmpRef: ComponentRef<IncidentViewComponent>;
  isViewInitialized: boolean = true;
  constructor(private incidentService : IncidentsService, _httpClient: HttpClient, private resolver: ComponentFactoryResolver, private conService : ConnexionService) {

  }

  ngOnInit() {
    this.nom = this.conService.nom;
    
    this.incidentSubscription = this.incidentService.incidentsSubject.subscribe(
      (incidents:any[])=>{
        console.log("incidents",incidents)
        this.incidents = incidents;
        if(this.incidents.length!=0){
          this.isIncidents = false;
        }
        if(this.incidents.length==0){
          this.isIncidents = true;
        }
        this.tIncident = this.incidents.length;
        this.sIncident = this.incidentService.sIncident;
        this.pIncident = this.incidentService.pIncident;
      }
    );
    this.incidentService.emitIncidentSubject();
  }

  ngOnDestroy(): void {
    this.incidentSubscription.unsubscribe();
  }

}
