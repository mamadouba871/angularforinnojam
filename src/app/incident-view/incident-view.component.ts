import { Component, OnInit, Input, ComponentRef } from '@angular/core';
import { Subscription, BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { IncidentsService } from '../services/incidents-service';
import { ConnexionService } from '../services/connexion-service';

@Component({
  selector: 'app-incident-view',
  templateUrl: './incident-view.component.html',
  styleUrls: ['./incident-view.component.css']
})
export class IncidentViewComponent implements OnInit {

  @Input() probabilite: number;
  @Input() message:string;
  @Input() id:number;
  @Input() index:number;
  idPersonne:number;

  hide:string="visible";
  
  constructor(private http: HttpClient, private incidentService : IncidentsService, private conService : ConnexionService) { }

  secondes: number;
  counterSubscription: Subscription;
  dateObj;
  hours; 
  minutes; 
  seconds;
  timeString;
  camera:string;//="Camera n°"+Math.floor(Math.random() * 100); 

  ngOnInit(){
    this.idPersonne = this.conService.id;
    const counter = Observable.interval(1000);
    this.counterSubscription = counter.subscribe(
      (value) => {
        this.secondes = value;
        this.dateObj = new Date(this.secondes * 1000); 
            this.hours = this.dateObj.getUTCHours(); 
            this.minutes = this.dateObj.getUTCMinutes(); 
            this.seconds = this.dateObj.getSeconds(); 
  
            this.timeString = this.hours.toString().padStart(2, '0') 
                + ':' + this.minutes.toString().padStart(2, '0') 
                + ':' + this.seconds.toString().padStart(2, '0'); 
      },
      (error) => {
        console.log('Uh-oh, an error occurred! : ' + error);
      },
      () => {
        console.log('Observable complete!');
      }
    );
    this.probabilite = Number.parseFloat(this.message.split(",")[4].split(":")[1].split("}")[0]);
    this.camera = this.message.split(",")[5].split(":")[1].split("}")[0];
    //this.camera = null?(this.message.split(",")[5].split(":")[1].split("}")[0]):"Origine inconnue";
    this.id = Number.parseFloat(this.message.split(",")[0].split(":")[1]);
    console.log("proba--------- : "+this.probabilite);
  }
  signaler(){
    this.hide="hidden";
    console.log(this.id);
    const url = 'http://localhost:5555/incident/'+this.id+'/'+this.idPersonne+'/false'
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'apllication/json',
        'Access-Control-Allow-Origin':'*'
      })
    };
    this.http.put(url, null, httpOptions).subscribe((response) => {
      this.incidentService.deleteIncident(this.index);
      this.incidentService.sIncident = this.incidentService.sIncident+1;
      console.log(this.idPersonne);
      this.incidentService.emitIncidentSubject();
      console.log("Response : ", response);
   },
   (error) => {
      console.error("Erreur : ", error);
   });
  }
  prendreCharge(){
    console.log(this.id);
    const url = 'http://localhost:5555/incident/'+this.id+'/'+this.idPersonne;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'apllication/json',
        'Access-Control-Allow-Origin':'*'
      })
    };
    this.http.put(url, null, httpOptions).subscribe((response) => {
      console.log("Response : ", response);
      this.incidentService.pIncident = this.incidentService.pIncident+1;
      this.incidentService.deleteIncident(this.index);
      console.log("Test pInc----------"+this.incidentService.pIncident);
      this.incidentService.emitIncidentSubject();
   },
   (error) => {
      console.error("Erreur : ", error);
   });
  }

  getHide():string{
    return this.hide;
  }

}
