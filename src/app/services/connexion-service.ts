import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/mergeMap'
import { Component, OnInit, Renderer2, Injectable } from '@angular/core';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import $ from 'jquery';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { NgForm } from '@angular/forms'

@Injectable({
    providedIn: 'root'
})
export class ConnexionService{

    constructor(private _httpClient: HttpClient) {
    }
    ngOnInit(): void {
    }

    isAuth = false;
    nom = "";
    id = 0;
  
    signIn() {
        return new Promise(
            (resolve, reject) => {
                  this.isAuth = true;
                  resolve(true);
            }
          );
    }
  
    signOut() {
      this.isAuth = false;
    }
}