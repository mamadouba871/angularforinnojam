import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/mergeMap'
import { Component, OnInit, Renderer2, Injectable, Input } from '@angular/core';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import $ from 'jquery';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class IncidentsService{
  
    incidentsSubject = new Subject<any[]>();
    private incidents:any[] = [];
    @Input()
    sIncident:number=0;
    @Input()
    pIncident:number=0;
    emitIncidentSubject() {
        console.log("COpie", this.incidents)
      this.incidentsSubject.next(this.incidents.slice());
    }
    private serverUrl = 'http://localhost:5555/innojam/socket-connect'
    //private title = 'WebSockets chat';
    private stompClient;

    constructor(private _httpClient: HttpClient) {
        
        //this.incidents.push({"id":79,"status":0,"personne":null,"isincident":0,"probabilite":0.91})
        //this.incidents.push({"id":79,"status":0,"personne":null,"isincident":0,"probabilite":0.95});
        console.log("sdf")
        this.emitIncidentSubject();
        this.initializeWebSocketConnection();
        this.emitIncidentSubject();
    }
    initializeWebSocketConnection(){
        let ws = new SockJS(this.serverUrl);
        this.stompClient = Stomp.over(ws);
        let that = this;
        this.stompClient.connect({}, function(frame) {
            that.stompClient.subscribe("/socket-send", (message) => {
                if(message.body) {
                    if(that.incidents==null){
                        that.incidents = new Array();
                    }
                    that.incidents.push((message.body).replace(/\"([^(\")"]+)\":/g,"$1:"));
                    that.emitIncidentSubject();
                    console.log("json : ----"+that.incidents[0].split(",")[4].split(":")[1].split("}")[0]);
                    console.log("proba service ---------"+(message.body).probabilite)
                }
            });
        });
    }

    getIncidentById(id: number) {
      const incident = this.incidents.find(
        (s) => {
          return s.id === id;
        }
      );
      return incident;
    }
    ngOnInit(): void { 

    }
    deleteIncident(id:number){
        this.incidents.splice(id,1);
        console.log("--------"+id);
    }
}