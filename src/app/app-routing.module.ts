import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { ListeIncidentsComponent } from './liste-incidents/liste-incidents.component';
import { AuthGuard } from './services/auth-guard.service';
import { ConnexionComponent } from './connexion/connexion.component';
import { InscriptionComponent } from './inscription/inscription.component';


const routes: Routes = [
  { path: '', component: ConnexionComponent },
  {path:'incidents', canActivate: [AuthGuard], component:ListeIncidentsComponent},
  {path:'inscription', component:InscriptionComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
