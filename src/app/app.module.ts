import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListeIncidentsComponent } from './liste-incidents/liste-incidents.component';
import { IncidentViewComponent } from './incident-view/incident-view.component';
import { IncidentsService } from './services/incidents-service';
import { ConnexionComponent } from './connexion/connexion.component';
import { ConnexionService } from './services/connexion-service';
import { AuthGuard } from './services/auth-guard.service';
import { InscriptionComponent } from './inscription/inscription.component';

@NgModule({
  declarations: [
    AppComponent,
    ListeIncidentsComponent,
    IncidentViewComponent,
    ConnexionComponent,
    InscriptionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [IncidentsService, ConnexionService, AuthGuard],
  bootstrap: [AppComponent],
  entryComponents: [IncidentViewComponent]
})
export class AppModule { }
