import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { ConnexionService } from '../services/connexion-service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {

  authStatus: boolean;
  isErreur:boolean = false;
  isIns:boolean = false;
  constructor(private http: HttpClient, private router: Router, private conService : ConnexionService) { }

  ngOnInit() {
    this.authStatus = this.conService.isAuth;
  }
  onSubmit(form: NgForm) {
    this.isIns = true;
    this.isErreur = false;
    console.log(form.value);
    const url = 'http://localhost:5555/personne';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'apllication/json',
        'Access-Control-Allow-Origin':'*'
      })
    };
    this.http.post(url, form.value, httpOptions).subscribe((response) => {
      if(response!=null){
        this.conService.signIn().then(
          () => {
            let propriete:any;
            propriete = response;
            console.log(propriete.nom);
            console.log('Registred successfully! : '+response);
            this.authStatus = this.conService.isAuth;
            this.conService.nom = propriete.nom+" "+propriete.prenom;
            this.conService.id = propriete.id;
            //setTimeout(function(){ this.router.navigate(['incidents']); }, 1000);
            this.router.navigate(['incidents']);
            
          }
        );
      }else{
        this.isIns = false;
        this.isErreur = true;
      }
      
      console.log("Response : ", response);
   },
   (error) => {
      this.isIns = false;
      this.isErreur = true;
      console.error("Erreur : ", error);
   });
}

}
